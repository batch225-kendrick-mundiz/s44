const posts = fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then(response => response.json());

function addPost() {
  const postInput = document.getElementById("postInput");
  const titleInput = document.getElementById("titleInput");
  posts.push({ title: titleInput.value, text: postInput.value });
  displayPosts();
}

function editPost(index) {
  const postInput = document.getElementById("postInput");
  const titleInput = document.getElementById("titleInput");
  posts[index] = { title: titleInput.value, text: postInput.value };
  displayPosts();
}

function deletePost(index) {
  posts.splice(index, 1);
  displayPosts();
}

function displayPosts() {
  const postsList = document.getElementById("postsList");
  postsList.innerHTML = "";
  for (let i = 0; i < posts.length; i++) {
    const post = posts[i];
    postsList.innerHTML +=
      "<li><h3>" +
      post.title +
      "</h3><p>" +
      post.text +
      "</p>" +
      " <button onclick='editPost(" +
      i +
      ")'>Edit</button> <button onclick='deletePost(" +
      i +
      ")'>Delete</button></li>";
  }
}